from recon.core.module import BaseModule
import json

class Module(BaseModule):

    meta = {
        'name': 'SSL SAN Lookup',
        'author': 'Zach Grace (@ztgrace) zgrace@403labs.com, rewritten by Bryan Onel (@BryanOnel86) onel@oneleet.com',
        'description': 'Uses the ssltools.com site to obtain the Subject Alternative Names for a domain. Updates the \'hosts\' table with the results.',
        'comments': (
            '2016-31-08: Rewritten because original endpoint and method not functioning anymore.'
        ),
        'query': 'SELECT DISTINCT domain FROM domains WHERE domain IS NOT NULL',
    }

    def module_run(self, domains):
        for domain in domains:
            self.heading(domain, level=0)
            url = 'http://www.ssltools.com/api/scan'

            html = self.request(url, method="POST", payload={'url': domain}).text
            parsed_json = json.loads(html)
            if not parsed_json['response']:
                self.output('SSL endpoint not reachable or response invalid for \'%s\'' % domain)
                continue
            if not parsed_json['response']['san_entries']:
                self.output('No Subject Alternative Names found for \'%s\'' % domain)
                continue
            san_entries = parsed_json['response']['san_entries']
            hosts = [x.strip() for x in san_entries if '*' not in x]
            for host in hosts:
                self.add_hosts(host)

